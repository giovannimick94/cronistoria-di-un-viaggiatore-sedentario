% GLOBAL PARAMETERS
date = #(strftime "%d-%m-%Y" (localtime (current-time))) 

% TO DO:
% Entra strumento a fiato sotto sfz di female voice
% Morphing delle female voice a gutturale


%TITLE
\markup \fill-line {
 \center-column{
\vspace #16
\fontsize #8 \line {Cronistoria di un viaggiatore sedentario}
\vspace #1
\fontsize #3 \line {per percussioni, sax soprano e pianoforte}
\vspace #2
\fontsize #5 \line {Giovanni Michelangelo D'Urso}
\vspace #1
\fontsize #1 \line {\date}
\vspace #2
  }
}
