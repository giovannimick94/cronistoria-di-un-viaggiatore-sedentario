date = #(strftime "%d-%m-%Y" (localtime (current-time))) 

\header {
  
                title = "Cronistoria di un viaggiatore sedentario"
                subtitle = "per percussioni, chitarra elettrica e pianoforte"
                composer = "Giovanni Michelangelo D'Urso"
                
                tagline = ""

}