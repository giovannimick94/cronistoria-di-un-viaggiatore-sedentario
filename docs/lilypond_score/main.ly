
% [1] VERSIONE ================================================
\version "2.22.2"

% [2] TEMPLATE PAGINA ===========================================
\include "templateA4.ly"
%\include"templateA3.ly"


%\book{
% [3] COVER PAGE =================================================
% https://coolsymbol.com/
  %\include "cover.ly"

%\include "blankpage.ly"
%\pageBreak

% [4] LEGENDA =================================================
%\include "legenda.ly"
%\pageBreak

% [4] HEADER =================================================
\include "header.ly"

% [5] BLOCCO NOTAZIONE (SCORE)===========================================

\include "Strumenti/chitarra_elettrica.ly"
%\include "Strumenti/Sax_soprano.ly"
\include "Strumenti/pianoforte.ly"
\include "Strumenti/percussioni.ly"
\include "Strumenti/fixedmedia.ly"

%[6] BLOCCO IMPOSTAZIONE RIGHI===========================================
\include "ce_piano_perc_fixedmedia.ly"
%\include "sax_piano_perc_fixedmedia.ly"


%[7] IMPOSTAZIONE RIGHI===========================================
\score {
  <<
  \primorigo
  \secondorigo
  \terzorigo
  \quartorigo
  >>
}


%\include "mytag.ly"
%}