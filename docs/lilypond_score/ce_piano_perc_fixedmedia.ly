primorigo = \new Staff \with{
                    
                    instrumentName = "Chitarra Elettrica"
                    shortInstrumentName = "C.E."
                    midiInstrument = "overdriven guitar"
\consists "Page_turn_engraver"
}		

{\chitarraelettrica}

secondorigo = \new PianoStaff \with{
                    
                    instrumentName = "Pianoforte"
                    shortInstrumentName = "Pf."
                    midiInstrument = "electric piano 1"
\consists "Page_turn_engraver"
}		
{\pianoforte}

terzorigo = \new Staff \with{
                    
                    instrumentName = "Percussioni"
                    shortInstrumentName = "Perc."
                    midiInstrument = "woodblock"
                      \hide Clef
\consists "Page_turn_engraver"
}	
{\percussioni}

quartorigo = \new Staff \with{
                    
                    instrumentName = "Fixed Media"
                    shortInstrumentName = "F.M."
                   
\consists "Page_turn_engraver"
}	
{\fixedmedia}