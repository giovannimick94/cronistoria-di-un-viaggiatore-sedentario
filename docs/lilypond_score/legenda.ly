% Legenda
\markup {
  
  \center-column{
  \fontsize #8 \line {Legenda}
  \vspace #4
  \fontsize #2 \line {Percussioni}
  \vspace #2
  \line {gruppo di suoni ◧}
  \vspace #1
  \line {triangolo \epsfile #X #8 #"triangolo.eps"}
  \vspace #1
  \line {nota più acuta possibile}
  \vspace #1
}
  
  \center-column{
  \vspace #5
  \fontsize #2 \line {Sax Soprano}
  \vspace #2
  \line {con bocca aperta}
  \vspace #1
  \line {con bocca semiaperta}
  \vspace #1
  \line {con bocca chiusa}
}
  
  \center-column{
  \vspace #5
  \fontsize #2 \line {Pianoforte}
  \vspace #2
  \line {respirare producendo suono}
  \vspace #1
  \line {gutturale}
  \vspace #1
  \line {tosse}
  \vspace #1
  }
  
}